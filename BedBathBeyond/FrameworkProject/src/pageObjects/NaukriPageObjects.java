package pageObjects;

import helperClasses.BaseClass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class NaukriPageObjects extends BaseClass{
	
	
	public static WebElement getTxtEmail(){
		element = findElementImWait(driver, By.xpath("//input[@id='email']"), waitTime_10sec);
		return element;
	}
	
	public static WebElement getTxtPassword(){
		element = findElementFuWait(driver, By.xpath("(//input[@name='PASSWORD'])[1]"), waitTime_10sec);
		return element;
	}
	
	public static WebElement getTxtCPassword(){
		element = findElementImWait(driver, By.xpath("(//input[@name='CPASSWORD'])[1]"), waitTime_10sec);
		return element;
	}
	


}
