package helperClasses;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.google.common.base.Function;

public class BaseClass {
	
	@BeforeSuite(groups = { "Sanity", "Regression" })
	public void suite(){
		System.out.println("In BeforeSuite");
	}
	@BeforeTest(groups = { "Sanity", "Regression" })
	public void betest(){
		System.out.println("In BeforeTest");

	}
	
	@AfterSuite(groups = { "Sanity", "Regression" })
	public void suite1(){
		System.out.println("In After Suite");
	}
	@AfterTest(groups = { "Sanity", "Regression" })
	public void betest1(){
		System.out.println("In AfterTest");

	}
	

	public static WebDriver driver = null;
	public static WebElement element = null;
	public static int waitTime_10sec = 10;;


	public static void launchApp(){

		String browser = ResourceBundleManager.getProperty("browser");
		String appUrl =ResourceBundleManager.getProperty("url");

		if("firefox".equalsIgnoreCase(browser)){
			driver = new FirefoxDriver();
		}else if("chrome".equalsIgnoreCase(browser)){
			System.setProperty("webdriver.chrome.driver","C:/Softwares/chromedriver.exe");
			driver = new ChromeDriver();

		}else if("ie".equalsIgnoreCase(browser)){
			System.setProperty("webdriver.ie.driver","C:/Softwares/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}

		driver.get(appUrl);
		driver.manage().window().maximize();
	}


	public static WebElement findElementFuWait(final WebDriver driver, final By locator, final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(500, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return driver.findElement(locator);
			}
		});
	}

	public static WebElement findElementImWait(WebDriver driver, final By locator, final int timeout){
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return element;
	}
}

