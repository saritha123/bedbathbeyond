package testScripts;

import java.util.HashMap;

import helperClasses.BaseClass;
import helperClasses.ReadXlsDataHelper;

import org.openqa.selenium.Keys;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pageObjects.NaukriPageObjects;

public class PriorityExNaukri1 extends BaseClass{

	@BeforeClass
	public void launchApplication(){
		launchApp();
		System.out.println("In before Class");
	}


	@Test()
	public  void naukriTest() throws Exception{

	

			HashMap<String, String> hsMap =	ReadXlsDataHelper.getXlsRowDataAsHashMapObj("TestData", "Sheet1", "1");


			element = NaukriPageObjects.getTxtEmail();			
			//			element.sendKeys("rama.mullamgdfeewei@gmail.com");

			element.sendKeys(hsMap.get("UserName"));
			element.sendKeys(Keys.TAB);		


			element = NaukriPageObjects.getTxtPassword();
			//			element.sendKeys("xxxxxxxxxxxxxxxxxx");
			element.sendKeys(hsMap.get("Password"));
			element.sendKeys(Keys.TAB);		

			System.out.println("In NaukriTest");
	
	}

	@Test(dependsOnMethods ={"naukriTest"})
	public void naukriTest1(){

		element = NaukriPageObjects.getTxtCPassword();
		element.sendKeys("xxxxxxxxxxxxxxxxxxxxxxx");
		element.sendKeys(Keys.TAB);	

		System.out.println("In NaukriTest1");


	}

	@AfterClass
	public void closeApp(){
		driver.quit();
		System.out.println("In After Class");
	}



}
