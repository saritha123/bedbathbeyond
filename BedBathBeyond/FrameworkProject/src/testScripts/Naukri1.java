package testScripts;

import java.util.HashMap;

import helperClasses.BaseClass;
import helperClasses.ReadXlsDataHelper;

import org.openqa.selenium.Keys;

import pageObjects.NaukriPageObjects;

public class Naukri1 extends BaseClass{

	public static void main(String[] args) {

		try{
			
		HashMap<String, String> hsMap =	ReadXlsDataHelper.getXlsRowDataAsHashMapObj("TestData", "Sheet1", "1");
		
			launchApp();
			element = NaukriPageObjects.getTxtEmail();			
//			element.sendKeys("rama.mullamgdfeewei@gmail.com");
			
			element.sendKeys(hsMap.get("UserName"));
			element.sendKeys(Keys.TAB);		


			element = NaukriPageObjects.getTxtPassword();
//			element.sendKeys("xxxxxxxxxxxxxxxxxx");
			element.sendKeys(hsMap.get("Password"));
			element.sendKeys(Keys.TAB);		

			element = NaukriPageObjects.getTxtCPassword();
			element.sendKeys(hsMap.get("Cpassword"));
			element.sendKeys(Keys.TAB);		

		
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
