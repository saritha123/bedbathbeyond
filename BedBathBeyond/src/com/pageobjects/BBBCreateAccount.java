package com.pageobjects;

import java.security.Key;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.helpclasses.BaseClass;

public class BBBCreateAccount extends BaseClass {
	
	public void lnkLogin(){
		element=driver.findElement(By.className("last"));  //linkText("Sign In / Account")) ;
		element.click();
		//}
		
		//element=objectfinderUsingExplicitWait(By.linkText("Log In")); //class="last" .//*[@id='topNavMenu']/ul/li[2]/a     
		//element.click();
		
	}
	
	public WebElement txtNewemail()
	{
		return objectfinderUsingExplicitWait(By.id("newEmail"));
	}
	
	public void btnCreateAcc(){
		element=objectfinderUsingExplicitWait(By.id("newEmailBtn"));
		element.submit();
	}
	
	public WebElement txtFirstName()
	{
		return objectfinderUsingExplicitWait(By.id("firstName"));
	}
	
	public WebElement txtLasttName()
	{
		return objectfinderUsingExplicitWait(By.id("lastName"));
	}
	
	public WebElement txtPrmPhone()
	{
		return objectfinderUsingExplicitWait(By.id("basePhoneFull"));
	}
	
	
	public WebElement txtCellPhone()
	{
		return objectfinderUsingExplicitWait(By.id("cellPhoneFull"));
	}
	
	public void selShareAcc()
	{
		element=driver.findElement(By.id("shareAccount"));
		 element.click();		  
		element.sendKeys(Keys.TAB);
	}
	
	public void selShowPsw()
	{
		element=driver.findElement(By.id("showPassword"));
		 		  
		element.click();
		element.sendKeys(Keys.TAB);
	}
	public WebElement txtPassword()
	{
		return driver.findElement(By.id("password"));
	}
	
	public WebElement txtCPassword()
	{
		return driver.findElement(By.id("cPassword"));
	}
	
	
	public WebElement txtPasswordValidations(){
		
		return driver.findElement(By.id("errorpassword"));
	}
	
	
	}
