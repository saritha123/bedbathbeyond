package com.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.helpclasses.BaseClass;

public class OrderPlacementPaypal extends BaseClass {
	
	public void lnkSelProduct(){

	element= driver.findElement(By.linkText("Sterling Silver Freshwater Cultured Pearl 4-Piece Jewelry Set"));
	element.click();
}
	public WebElement selQuantity(){
		
		return driver.findElement(By.id("quantity"));
	}
	
	public void addToCart(){
		 //Actions act = new Actions(driver);
		element=driver.findElement(By.name("btnAddToCart"));
		 Actions act = new Actions(driver);
		 act.moveToElement(element).perform();
		element.click();
	}
	
	public void lnkselgotCart(){
		element=driver.findElement(By.id("miniCart"));
		 Actions act = new Actions(driver);
		 act.moveToElement(element).perform();
		element.click();
	}
				
		public void lnkcart(){
			
		
		element=driver.findElement(By.id("cartItems"));//findElement(By.linkText("Cart"));
		 //Actions act = new Actions(driver);
		 //act.moveToElement(element).perform();
		element.click();
		}
	
	
	public void lnkPayPal(){
		
		   
		element=driver.findElement(By.className("paypalCheckoutButton"));//class="paypalCheckoutButton"
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");//scroll down //for scroll up (0,-250)
		String txt=element.getText();
		element.click();
		//String url=driver.getCurrentUrl();
		//driver.navigate().to(url);
		
		}
	
		public void pagePayPalclk(){
			
			String winpaypal="https://www.paypal.com";
			//driver.switchTo().window(winpaypal);
			driver.navigate().forward();
		}
	
	public WebElement txtEmail(){
		
		element= driver.findElement(By.xpath("//*[@id='login_emaildiv']"));//name("login_email"));  //id("email"));
		//.//*[@id='login_emaildiv']
	//	<div class="textInput" id="login_emaildiv">
		//Actions act = new Actions(driver);
		//act.moveToElement(element).perform();
		element.click();
		return element;
	}
	
	public WebElement txtPassword(){
		
		return driver.findElement(By.id("password"));
	}
	
	public void lnkLogin(){
		element=driver.findElement(By.id("btnLogin"));
		element.click();
	}
	 
	
	
	// https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-7UJ42374R8673401U&_requestid=912209#/checkout/login
}
