package com.helpclasses;

	import java.net.MalformedURLException;
	import java.net.URL;

	import org.openqa.selenium.JavascriptExecutor;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.firefox.FirefoxDriver;
	import org.openqa.selenium.firefox.FirefoxProfile;
	import org.openqa.selenium.remote.DesiredCapabilities;
	import org.openqa.selenium.remote.RemoteWebDriver;
	import org.openqa.selenium.support.ui.ExpectedCondition;
	import org.openqa.selenium.support.ui.Wait;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.annotations.AfterMethod;
	import org.testng.annotations.BeforeMethod;


	 
	public class GridTestBase {	
		
		public String browserName=null;
	 
	    protected  ThreadLocal<RemoteWebDriver> threadDriver = null;
	 
	    @BeforeMethod
	    public void setUp() throws MalformedURLException {
	 
	        threadDriver = new ThreadLocal<RemoteWebDriver>();
	        DesiredCapabilities dc = new DesiredCapabilities();
	        
	        if("firefox".equalsIgnoreCase(browserName)){
	        dc.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
	        threadDriver.set(new RemoteWebDriver(new URL("http://localhost:4443/wd/hub"), dc));
	        }else if("chrome".equalsIgnoreCase(browserName)){
	        
	        dc = DesiredCapabilities.chrome();
	        dc.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
	        dc.setPlatform(org.openqa.selenium.Platform.ANY);
	        System.setProperty("webdriver.chrome.driver", "C://Softwares//chromedriver.exe");
	        URL url = new URL("http://localhost:4443/wd/hub");
	        threadDriver.set( new RemoteWebDriver(url, dc));
	        }
	    }
	 
	   public  WebDriver driver() {
	 	
	       return threadDriver.get();
	    }
	 
	   @AfterMethod
	    public void closeBrowser() {
//	        getDriver().quit();
//	    	driver().quit();
	 
	    }
	    
	  
	}



