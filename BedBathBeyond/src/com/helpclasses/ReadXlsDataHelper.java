package com.helpclasses;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
//import org.apache.xpath.operations.String;

//import core.libs.Log;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
/**
 * @Class: ReadXlsDataHelper
 * @Description: This class is a helper class used to read the data from the excel file
 * @author Rama Reddy Mullamgi
 */
public class ReadXlsDataHelper {	
	private String inputFile;
	private static int rowValue=0;
	private static int columnvalue=0;

	/**
	 * @param inputFile
	 * Sets the input file
	 */
	public void setInputFile(String inputFile)
	{
		this.inputFile = inputFile;
	}
//	Alter alt=driver.getwindowdriver().alert();

	/**
	 * @param filename
	 * @param sheetName
	 * @param rowValue 
	 * @return Map<String,String> this will read the data and it even reads empty cell
	 * @throws BiffException
	 * @throws IOException
	 */


			public static HashMap<String,String> getXlsRowDataAsMap(String filename, String sheetName,String rowValue) throws BiffException, IOException
			{

			HashMap<String,String> hmRowData = new HashMap<String,String>();
			try{ 


			/*getting the complete path of the file*/
			//String pathAppend=Constants.TESTDATA_FILELOC;
//			String pathAppend=ResourceBundleManager.getProperty("TESTDATA_FILELOC_KEY");
			String pathAppend = "C:\\SarithaWs\\BedBathBeyond\\src\\com\\testdata";
			String completepath=pathAppend+"/"+filename+".xls";
			System.out.println(completepath);

			Sheet sheetObject = null;
			Workbook workBookObj = null;
			Cell cellObj = null;
			int columnNumber = 0;
			int rowNumber = 0;

			//RowNumber of Column Headings
			int rowHeader=rowNumber;


			workBookObj = Workbook.getWorkbook(new File(completepath));

//			        logScriptInfo("[INFO]WorkBook Object Created:"+workBookObj);
			sheetObject = workBookObj.getSheet(sheetName);

//			        Log.logScriptInfo("[INFO] Sheet Object Created:"+sheetObject);
			cellObj = sheetObject.findCell(rowValue);
//			        Log.logScriptInfo("[INFO]Cell Object Created:"+cellObj);
			columnNumber = cellObj.getColumn();
			rowNumber = cellObj.getRow(); 
			//getting no of columns in the sheet
			int totalColumns = sheetObject.getColumns();

			for (int i = columnNumber + 1; i < totalColumns; i++)
			{
			if(sheetObject.getCell(i, rowNumber).getContents()==null || sheetObject.getCell(i, rowNumber).getContents()=="")
			{
			hmRowData.put(sheetObject.getCell(i, rowHeader).getContents().trim(),sheetObject.getCell(i, rowNumber).getContents());
			}
			if(sheetObject.getCell(i, rowNumber).getContents()!=null && sheetObject.getCell(i,rowNumber).getContents()!="")
			{
			hmRowData.put(sheetObject.getCell(i, rowHeader).getContents().trim(),sheetObject.getCell(i, rowNumber).getContents());
			}

			}
//			        Log.logScriptInfo("[INFO]sucessfulCompletion:getXlsRowData");
			return hmRowData;
			} catch(Exception e){

			}
			return hmRowData;
			}



			

}
