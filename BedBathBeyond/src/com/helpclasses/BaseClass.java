package com.helpclasses;



	
	import java.io.File;
	import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

	public class BaseClass {

		public static WebDriver driver;
		public static WebElement element;
		public String browserName=null;
		 protected  ThreadLocal<RemoteWebDriver> threadDriver = null;
		
	//**********************************************************************************//
		
		/*public static void launchApp()
		{
		   driver=new FirefoxDriver();
			driver.get("http://www.Barnesandnoble.com");
			driver.manage().window().maximize();
			
		}*/
		
		
		
		//********************************************************************************//
		/*public static void launchApp(String url){		
			driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().window().maximize();		
		}*/
		
		
		public  void launchApp(){
			
			 System.out.println(ResourceBundleManager.getProperty("browser"));
			    
			    if("true".equalsIgnoreCase(ResourceBundleManager.getProperty("GridExecution"))){
			     
			     driver= threadDriver.get();
			     driver.get(ResourceBundleManager.getProperty("url"));
			     driver.manage().window().maximize();
			   
			    }
			    else{
				      if("firefox".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
				       driver = new FirefoxDriver(); 
				      }else if("chrome".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
				       System.setProperty("webdriver.chrome.driver", "/Users/sudhirmarni/Teju/javaseltraining/barnes&Noble-selenium");
				       driver = new ChromeDriver();
				      }else if("ie".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
				       System.setProperty("webdriver.ie.driver", "C:\\Softwares\\IEDriverServer_x64_2.48.0\\IEDriverServer.exe");
				       driver = new InternetExplorerDriver();
				      }
				      driver.get(ResourceBundleManager.getProperty("url"));
				      driver.manage().window().maximize();
				     }
				  }
				 			
			
			
			
		 
		    @BeforeMethod
		   
		    public void setUp() throws MalformedURLException {
			 
				
		    	if("true".equalsIgnoreCase(ResourceBundleManager.getProperty("GridExecution"))){
		 		   threadDriver = new ThreadLocal<RemoteWebDriver>();
		 		   DesiredCapabilities dc = new DesiredCapabilities();
		        	        
		        if("firefox".equalsIgnoreCase(browserName)){
		        dc.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
		        threadDriver.set(new RemoteWebDriver(new URL("http://localhost:4443/wd/hub"), dc));
		        }else if("chrome".equalsIgnoreCase(browserName)){
		    		
		        	//ChromeOptions co = new ChromeOptions();
		    		//here "--start-maximized" argument is responsible to maximize chrome browser
		    		//co.addArguments("--start-maximized");
		    		
		        dc = DesiredCapabilities.chrome();
		        dc.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
		        dc.setPlatform(org.openqa.selenium.Platform.ANY);
		        System.setProperty("webdriver.chrome.driver", "C://Softwares//chromedriver.exe");
		        URL url = new URL("http://localhost:4443/wd/hub");
		        threadDriver.set( new RemoteWebDriver(url, dc));
		        }
		        threadDriver.get().manage().window().maximize();
		        
		    }}
		 
		/****************************************************************** 
		   public  WebDriver driver() {
		 	
		       return threadDriver.get();
		    }
		 
		   @AfterMethod
		    public void closeBrowser() {
//		        getDriver().quit();
//		    	driver().quit();
		 
		    }

		**********************************************************/	
		
		
		public static WebElement objectfinderUsingExplicitWait(By xpath, int waitTime){		
			
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(xpath));
					return element;
		}
		
		public static WebElement objectfinderUsingExplicitWait(By xpath){		
			
			WebDriverWait wait = new WebDriverWait(driver, 30);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(xpath));
					return element;
		}
		
		
		
		/*public static void takeScreeShot(String screenShotName) throws IOException{
			
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			// Now you can do whatever you need to do with it, for example copy somewhere
			FileUtils.copyFile(scrFile, new File("c:\\tmp\\"+screenShotName+".png"));
		}*/
		
//**********************************************************************************************************************************
		


		
		 
		 
		 
		/* public  void launchApp(){
		    System.out.println(ResourceBundleManager.getProperty("browser"));
		    
		    if("true".equalsIgnoreCase(ResourceBundleManager.getProperty("GridExecution"))){
		     
		     driver= threadDriver.get();
		     driver.get(ResourceBundleManager.getProperty("url"));
		     driver.manage().window().maximize();
		   
		    }else{
		      if("firefox".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		       driver = new FirefoxDriver(); 
		      }else if("chrome".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		       System.setProperty("webdriver.chrome.driver", "/Users/sudhirmarni/Teju/javaseltraining/barnes&Noble-selenium");
		       driver = new ChromeDriver();
		      }else if("ie".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		       System.setProperty("webdriver.ie.driver", "C:\\Softwares\\IEDriverServer_x64_2.48.0\\IEDriverServer.exe");
		       driver = new InternetExplorerDriver();
		      }
		      driver.get(ResourceBundleManager.getProperty("url"));
		      driver.manage().window().maximize();
		     }
		  }
		  
		 
		*/ public static void quitApp(){
		  driver.quit();
		 }
		 
		 
		 
		 
		
		 /******************************************************
		@BeforeMethod
		 public void setUp() throws MalformedURLException {

		  if("true".equalsIgnoreCase(ResourceBundleManager.getProperty("GridExecution"))){
		   threadDriver = new ThreadLocal<RemoteWebDriver>();
		   DesiredCapabilities dc = new DesiredCapabilities();

		   if("firefox".equalsIgnoreCase(browserName)){
		    dc.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
		    threadDriver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc));
		   }else if("chrome".equalsIgnoreCase(browserName)){

		    dc = DesiredCapabilities.chrome();
		    dc.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
		    dc.setPlatform(org.openqa.selenium.Platform.ANY);
		    System.setProperty("webdriver.chrome.driver", "/Users/sudhirmarni/Teju/seltraining/chromedriver");
		    URL url = new URL("http://localhost:4444/wd/hub");
		    threadDriver.set( new RemoteWebDriver(url, dc));
		   }
***************************************************************/
		  /*else{
		   if("firefox".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		        driver = new FirefoxDriver(); 
		       }else if("chrome".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		        System.setProperty("webdriver.chrome.driver", "/Users/sudhirmarni/Teju/javaseltraining/barnes&Noble-selenium");
		        driver = new ChromeDriver();
		       }else if("ie".equalsIgnoreCase(ResourceBundleManager.getProperty("browser"))){
		        System.setProperty("webdriver.ie.driver", "C:\\Softwares\\IEDriverServer_x64_2.48.0\\IEDriverServer.exe");
		        driver = new InternetExplorerDriver();
		       }
		       driver.get(ResourceBundleManager.getProperty("url"));
		       driver.manage().window().maximize();
		      }*/

		  }
		  
		 
		 
		
		


