package com.helpclasses;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class GridLaunch extends GridTestBase {
	
	@Parameters({ "browserN" })
	@BeforeClass()
	public void setBrowser(String browserN){
//		browserName="chrome";
		browserName=browserN;
		
	}
	@BeforeTest
	public  void launchApp(){
			System.out.println(ResourceBundleManager.getProperty("browser"));
			driver().get(ResourceBundleManager.getProperty("url"));
			driver().manage().window().maximize();
	    

}
}
