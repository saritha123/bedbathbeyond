package com.testscript;

import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.helpclasses.BaseClass;
import com.helpclasses.ReadXlsDataHelper;
import com.pageobjects.BBBCreateAccount;
import com.pageobjects.BedBathBeyondHome;

public class BBcreateAccountTest extends BaseClass {
	
//	@Test
	
	public void bbbAccountRegistry()  
	{
	try {
	
	
		
		BedBathBeyondHome BNHObj=new BedBathBeyondHome();
		BBBCreateAccount BBCAobj=new BBBCreateAccount();
		HashMap<String,String> BBACDobj = new HashMap<String,String>();
	
		
		launchApp();
	//BNHObj.lnkNavbar();
		Thread.sleep(3000);
		
		BBCAobj.lnkLogin();
		
		
		BBACDobj=ReadXlsDataHelper.getXlsRowDataAsMap("Book","Sheet1","1");
		
		BBCAobj.txtNewemail().sendKeys(BBACDobj.get("Email"));
		BBCAobj.btnCreateAcc();
		Thread.sleep(3000);
		
		
		BBCAobj.txtFirstName().sendKeys(BBACDobj.get("First Name"));
		BBCAobj.txtFirstName().sendKeys(Keys.TAB);
		
		
		BBCAobj.txtLasttName().sendKeys(BBACDobj.get("Last Name"));
		BBCAobj.txtLasttName().sendKeys(Keys.TAB);
		
		BBCAobj.txtPrmPhone().sendKeys(BBACDobj.get("PrimaryPhone"));
		BBCAobj.txtPrmPhone().sendKeys(Keys.TAB);
		
		BBCAobj.txtCellPhone().sendKeys(BBACDobj.get("MobilePhone"));
		BBCAobj.txtCellPhone().sendKeys(Keys.TAB);
		Thread.sleep(2000);
			
		
		BBCAobj.selShareAcc();
		Thread.sleep(2000);
		
		BBCAobj.selShowPsw();
		Thread.sleep(2000);
		
		BBCAobj.txtPassword().sendKeys(BBACDobj.get("Password"));
		//BBCAobj.selShowPsw();
		BBCAobj.txtCPassword().sendKeys(BBACDobj.get("ConPassword"));
		
		
		
		
		quitApp();

}catch(Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}

	
	
	
	@Test
	 
	public void testCreateAccountPassword(){
	 
		 try
		 {
		 BedBathBeyondHome BNHObj=new BedBathBeyondHome();
			
		 BBBCreateAccount BBCAobj=new BBBCreateAccount();
			
		 HashMap<String,String> BBACDobj = new HashMap<String,String>();
			
		 launchApp();
			
		 BBACDobj=ReadXlsDataHelper.getXlsRowDataAsMap("Book","Sheet1","1");
			
		 BBCAobj.lnkLogin();
		
		 BBCAobj.txtNewemail().sendKeys(BBACDobj.get("Email"));
			
		 BBCAobj.btnCreateAcc();
			
		 ArrayList<String> expectedList=new ArrayList<String>();
	  expectedList.add("Please enter at least 8 characters.");
	  expectedList.add("Must contain a digit.");
	  expectedList.add("Must contain an uppercase letter (A-Z).");
	  expectedList.add("Spaces are not permitted.  Please re-enter the value.");
	  expectedList.add("Must contain a lowercase letter (a-z).");
	  expectedList.add("Password must not contain your last name"); 
	  expectedList.add("Password must not contain your first name");
	  
	  
	  
	  
	  
	  ArrayList<String> actualList=new ArrayList<String>();
	  
	  BBCAobj.txtPassword().sendKeys("test");
	  BBCAobj.txtPassword().sendKeys(Keys.TAB);
	  WebElement errorMsgElement=null;
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	  String errorMsg=errorMsgElement.getText();
	  System.out.println(errorMsg);
	  actualList.add(errorMsg);
	  
	  BBCAobj.txtPassword().sendKeys("ittesting");
	  BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   actualList.add(errorMsg);
	   
	   
	   BBCAobj.txtPassword().sendKeys("test1234");
	   BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   System.out.println(errorMsg);
	   actualList.add(errorMsg);
	   
	 
	   BBCAobj.txtPassword().sendKeys("Test1234 5");
	   BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   System.out.println(errorMsg);
	   actualList.add(errorMsg);
	   
	  
	   BBCAobj.txtPassword().sendKeys("TEST1234");
	   BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   System.out.println(errorMsg);
	   actualList.add(errorMsg);
	  
	  

	   BBCAobj.txtPassword().sendKeys("Nandyala1");
	   BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   System.out.println(errorMsg);
	   actualList.add(errorMsg);
	  
	   
	   
	   BBCAobj.txtPassword().sendKeys("Saritha1");
	   BBCAobj.txtPassword().sendKeys(Keys.TAB);
	   errorMsgElement=BBCAobj.txtPasswordValidations();
	   errorMsg=errorMsgElement.getText();
	   System.out.println(errorMsg);
	   actualList.add(errorMsg);
	  
	   
	   
	  System.out.println(errorMsg);
	  
	 Assert.assertEquals(actualList, expectedList);//(errorMsg,"Please enter at least 8 characters.");
	  
	  
	  
	  
	  driver.quit();
	  
	 } catch (Exception e) {
	  // TODO: handle exception
	  e.printStackTrace();
	 }
	 }	 




}
